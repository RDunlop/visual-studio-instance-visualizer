﻿using NUnit.Framework;
using Roduso.VisualStudioInstanceVisualizer.Specifications.Units.InstanceVisualizerViewModelSpecs;
using Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling;

namespace Roduso.VisualStudioInstanceVisualizer.Specifications.CloseSolution
{
    [TestFixture]
    public class When_a_solution_is_closed : InstanceVisualizerViewModelContextSpecification
    {
        [Test]
        public void An_informative_configuration_should_be_applied()
        {
            Assert.That(InstanceVisualizerViewModel.Configuration, Is.TypeOf<InformativeConfiguration>());
        }

        protected override void SetUpMockData()
        {
        }

        protected override void SimulateExternalActorsBehaviours()
        {
            SimulateCreatingTheInformativeConfiguration();
        }

        protected override void InitializeStateOnClassUnderTest()
        {
            CreateInstanceVisualizerViewModel();
        }

        protected override void Act()
        {
            InstanceVisualizerViewModel.SolutionClosed();
        }
    }
}