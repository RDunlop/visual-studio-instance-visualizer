﻿using NUnit.Framework;

using Roduso.VisualStudioInstanceVisualizer.Specifications.Units.InstanceVisualizerViewModelSpecs;
using Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling;

namespace Roduso.VisualStudioInstanceVisualizer.Specifications.Activation.NoSolutionOpen
{
    [TestFixture]
    public class When_the_visualizer_is_activated : InstanceVisualizerViewModelContextSpecification
    {
        [Test]
        public void An_informative_configuration_should_be_shown()
        {
            Assert.That(InstanceVisualizerViewModel.Configuration, Is.TypeOf<InformativeConfiguration>());
        }      

        protected override void SetUpMockData()
        {
            GivenNoSolutionIsOpen();
        }

        protected override void SimulateExternalActorsBehaviours()        
        {            
            SimulateCreatingTheInformativeConfiguration();
        }

        protected override void InitializeStateOnClassUnderTest()
        {
            CreateInstanceVisualizerViewModel();
        }

        protected override void Act()
        {
            InstanceVisualizerViewModel.Activate(CurrentSolutionFilePath);
        }
    }
}