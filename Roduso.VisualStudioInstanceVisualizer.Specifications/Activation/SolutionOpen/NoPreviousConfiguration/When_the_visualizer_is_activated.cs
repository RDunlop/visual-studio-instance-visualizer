﻿using NUnit.Framework;
using Roduso.VisualStudioInstanceVisualizer.Specifications.Units.InstanceVisualizerViewModelSpecs;
using Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling;

namespace Roduso.VisualStudioInstanceVisualizer.Specifications.Activation.SolutionOpen.NoPreviousConfiguration
{
    [TestFixture]
    public class When_the_visualizer_is_activated : InstanceVisualizerViewModelContextSpecification
    {
        [Test]
        public void A_default_configuration_should_be_applied()
        {
            Assert.That(InstanceVisualizerViewModel.Configuration, Is.TypeOf<DefaultConfiguration>());
        }      

        protected override void SetUpMockData()
        {
            GivenASolutionFullName();
            GivenAConfigurationFullName();
            GivenASolutionIsOpen();
            GivenNoConfigurationIsPreviouslySaved();
        }

        protected override void SimulateExternalActorsBehaviours()        
        {            
            SimulateThatTheConfigurationFilePathDoesNotExists();
            SimulateCreatingTheDefaultConfiguration();
            SimulateSavingTheConfiguration();
        }

        protected override void InitializeStateOnClassUnderTest()
        {
            CreateInstanceVisualizerViewModel();
        }

        protected override void Act()
        {            
            InstanceVisualizerViewModel.Activate(CurrentSolutionFilePath);
        }
    }
}