﻿using NUnit.Framework;
using Roduso.VisualStudioInstanceVisualizer.Specifications.Units.InstanceVisualizerViewModelSpecs;
using Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling;

namespace Roduso.VisualStudioInstanceVisualizer.Specifications.Activation.SolutionOpen.PreviousConfiguration
{
    [TestFixture]
    public class When_the_visualizer_is_activated : InstanceVisualizerViewModelContextSpecification
    {
        [Test]
        public void The_solutions_configuration_should_be_applied()
        {
            Assert.That(InstanceVisualizerViewModel.Configuration,
                        Is.TypeOf<Configuration>()
                        .And.Not.TypeOf<InformativeConfiguration>()
                        .And.Not.TypeOf<DefaultConfiguration>());
        }      

        protected override void SetUpMockData()
        {
            GivenASolutionFullName();
            GivenAConfigurationFullName();
            GivenASolutionIsOpen();
            GivenAConfigurationIsPreviouslySaved();
        }

        protected override void SimulateExternalActorsBehaviours()        
        {            
            SimulateThatTheConfigurationFilePathExists();
            SimulateLoadingTheConfiguration();
        }

        protected override void InitializeStateOnClassUnderTest()
        {
            CreateInstanceVisualizerViewModel();
        }

        protected override void Act()
        {            
            InstanceVisualizerViewModel.Activate(CurrentSolutionFilePath);
        }
    }
}