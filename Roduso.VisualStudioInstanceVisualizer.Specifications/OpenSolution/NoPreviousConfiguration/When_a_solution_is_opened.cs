﻿using Moq;
using NUnit.Framework;
using Roduso.VisualStudioInstanceVisualizer.Specifications.Units.InstanceVisualizerViewModelSpecs;
using Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling;

namespace Roduso.VisualStudioInstanceVisualizer.Specifications.OpenSolution.NoPreviousConfiguration
{
    [TestFixture]
    public class When_a_solution_is_opened : InstanceVisualizerViewModelContextSpecification
    {
        [Test]
        public void First_the_default_configuration_should_be_created()
        {
            ConfigurationFactoryMock.Verify(x => x.CreateDefaultConfiguration(It.IsAny<string>()), Times.Once());
        }

        [Test]
        public void Second_the_configuration_should_be_saved()
        {
            ConfigurationRepositoryMock.Verify(x => x.Save(It.IsAny<string>(), It.IsAny<Configuration>()), Times.Once());
        }

        [Test]
        public void A_default_configuration_should_be_applied()
        {
            Assert.That(InstanceVisualizerViewModel.Configuration, Is.TypeOf<DefaultConfiguration>());
        }

        protected override void SetUpMockData()
        {
            GivenASolutionFullName();
            GivenAConfigurationFullName();
            GivenASolutionIsOpen();
            GivenNoConfigurationIsPreviouslySaved();
        }

        protected override void SimulateExternalActorsBehaviours()
        {
            SimulateThatTheConfigurationFilePathDoesNotExists();
            SimulateCreatingTheDefaultConfiguration();
            SimulateSavingTheConfiguration();
        }

        protected override void InitializeStateOnClassUnderTest()
        {
            CreateInstanceVisualizerViewModel();
        }

        protected override void Act()
        {
            InstanceVisualizerViewModel.SolutionChanged(SolutionFullName);
        }
    }
}