﻿using Moq;
using NUnit.Framework;
using Roduso.VisualStudioInstanceVisualizer.Specifications.Units.InstanceVisualizerViewModelSpecs;
using Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling;

namespace Roduso.VisualStudioInstanceVisualizer.Specifications.OpenSolution.PreviousConfiguration
{
    [TestFixture]
    public class When_a_solution_is_opened : InstanceVisualizerViewModelContextSpecification
    {
        [Test]
        public void First_the_saved_configuration_should_be_loaded()
        {
            ConfigurationRepositoryMock.Verify(x => x.Load(ConfigurationFullName), Times.Once());
        }

        [Test]
        public void The_configuration_should_not_be_saved()
        {
            ConfigurationRepositoryMock.Verify(x => x.Save(ConfigurationFullName, Configuration), Times.Never());
        }

        [Test]
        public void The_solutions_configuration_should_be_applied()
        {
            Assert.That(InstanceVisualizerViewModel.Configuration, 
                Is.TypeOf<Configuration>()
                .And.Not.TypeOf<InformativeConfiguration>()
                .And.Not.TypeOf<DefaultConfiguration>());
        }

        protected override void SetUpMockData()
        {
            GivenASolutionFullName();
            GivenAConfigurationFullName();
            GivenASolutionIsOpen();
            GivenAConfigurationIsPreviouslySaved();
        }

        protected override void SimulateExternalActorsBehaviours()
        {
            SimulateThatTheConfigurationFilePathExists();
            SimulateLoadingTheConfiguration();
            SimulateSavingTheConfiguration();
        }

        protected override void InitializeStateOnClassUnderTest()
        {
            CreateInstanceVisualizerViewModel();
        }

        protected override void Act()
        {
            InstanceVisualizerViewModel.SolutionChanged(SolutionFullName);
        }
    }
}