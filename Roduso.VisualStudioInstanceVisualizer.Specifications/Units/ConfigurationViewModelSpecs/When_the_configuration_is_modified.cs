using Moq;
using NUnit.Framework;
using Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling;

namespace Roduso.VisualStudioInstanceVisualizer.Specifications.Units.ConfigurationViewModelSpecs
{
    [TestFixture]
    public class When_the_configuration_is_modified : ConfigurationViewModelContextSpecification
    {
        [Test]
        public void The_configuration_should_be_persisted()
        {
            ConfigurationRepositoryMock.Verify(x => x.Save(It.IsAny<string>(), It.IsAny<Configuration>()), Times.Once);
        }

        protected override void SetUpMockData()
        {
            ConfigurationRepositoryMock = new Mock<IConfigurationRepository>(MockBehavior.Strict);
        }

        protected override void SimulateExternalActorsBehaviours()
        {
            ConfigurationRepositoryMock.Setup(x => x.Save(It.IsAny<string>(), It.IsAny<Configuration>())).Verifiable();
        }

        protected override void InitializeStateOnClassUnderTest()
        {
            CreateConfigurationViewModel();
            AssignConfigurationToEdit();
        }

        protected override void Act()
        {
            SettingsViewModel.Configuration.ProjectName = "test";
        }
    }
}