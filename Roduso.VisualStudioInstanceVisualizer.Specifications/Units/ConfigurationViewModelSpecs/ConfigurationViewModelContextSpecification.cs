using Moq;
using Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling;
using Roduso.VisualStudioInstanceVisualizer.UserInterface.ViewModels;

namespace Roduso.VisualStudioInstanceVisualizer.Specifications.Units.ConfigurationViewModelSpecs
{
    public abstract class ConfigurationViewModelContextSpecification : ContextSpecification
    {
        protected ISettingsViewModel SettingsViewModel { get; set; }
        protected Mock<IConfigurationRepository> ConfigurationRepositoryMock { get; set; }

        protected override void CreateMocksOfExternalActors()
        {
            ConfigurationRepositoryMock = new Mock<IConfigurationRepository>(MockBehavior.Strict);
        }

        protected void CreateConfigurationViewModel()
        {
            SettingsViewModel = new SettingsViewModel(ConfigurationRepositoryMock.Object);
        }

        protected void AssignConfigurationToEdit()
        {
            SettingsViewModel.Initialize(new Configuration(), configurationFilePath: "any path");
        }
    }
}