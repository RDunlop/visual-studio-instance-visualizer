﻿using NUnit.Framework;

using Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling;

namespace Roduso.VisualStudioInstanceVisualizer.Specifications.Units.ConfigurationViewModelSpecs
{
    [TestFixture]
    public class When_the_configuration_view_is_created : ConfigurationViewModelContextSpecification
    {
        [Test]
        public void It_should_be_possible_to_initialize_the_view_with_a_configuration()
        {
            Assert.DoesNotThrow(() => SettingsViewModel.Initialize(new Configuration(), configurationFilePath: "any path"));
        }

        protected override void SetUpMockData()
        {
        }

        protected override void SimulateExternalActorsBehaviours()
        {
        }

        protected override void InitializeStateOnClassUnderTest()
        {
        }

        protected override void Act()
        {
            CreateConfigurationViewModel();
        }
    }
}