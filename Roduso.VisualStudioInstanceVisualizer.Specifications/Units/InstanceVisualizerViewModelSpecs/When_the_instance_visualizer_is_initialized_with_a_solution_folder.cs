using Moq;

using NUnit.Framework;

namespace Roduso.VisualStudioInstanceVisualizer.Specifications.Units.InstanceVisualizerViewModelSpecs
{
    [TestFixture]
    public class When_the_instance_visualizer_is_initialized_with_a_solution_folder : InstanceVisualizerViewModelContextSpecification
    {
        [Test]
        public void The_last_saved_configuration_should_be_loaded()
        {
            ConfigurationRepositoryMock.Verify(x => x.Load(ConfigurationFullName), Times.Once());
        }

        protected override void SetUpMockData()
        {
            GivenASolutionFullName();
            GivenAConfigurationFullName();
            GivenASolutionIsOpen();
            GivenAConfigurationIsPreviouslySaved();
        }

        protected override void SimulateExternalActorsBehaviours()
        {
            SimulateThatTheConfigurationFilePathExists();
            SimulateLoadingTheConfiguration();
            SimulateSavingTheConfiguration();
        }

        protected override void InitializeStateOnClassUnderTest()
        {
            CreateInstanceVisualizerViewModel();
        }

        protected override void Act()
        {
            ActivateTheInstanceVisualizerViewModel();
        }
    }
}