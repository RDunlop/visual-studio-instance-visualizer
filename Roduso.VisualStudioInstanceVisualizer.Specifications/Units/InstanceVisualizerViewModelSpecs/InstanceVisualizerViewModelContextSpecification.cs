using Moq;

using Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling;
using Roduso.VisualStudioInstanceVisualizer.UserInterface.FileHandling;
using Roduso.VisualStudioInstanceVisualizer.UserInterface.ViewModels;

namespace Roduso.VisualStudioInstanceVisualizer.Specifications.Units.InstanceVisualizerViewModelSpecs
{
    public abstract class InstanceVisualizerViewModelContextSpecification : ContextSpecification
    {
        protected IInstanceVisualizerViewModel InstanceVisualizerViewModel { get; set; }

        protected Mock<IFileExistenceChecker> FileExistenceCheckerMock { get; set; }
        protected Mock<IConfigurationRepository> ConfigurationRepositoryMock { get; set; }
        protected Mock<IConfigurationFactory> ConfigurationFactoryMock { get; set; }
        protected Mock<ISettingsViewModel> SettingsViewModelMock { get; set; }

        public string CurrentSolutionFilePath { get; set; }
        public string SolutionName { get; set; }
        protected string SolutionFullName { get; set; }
        protected string ConfigurationFullName { get; private set; }
        protected Configuration Configuration { get; set; }

        protected override void CreateMocksOfExternalActors()
        {
            FileExistenceCheckerMock = new Mock<IFileExistenceChecker>(MockBehavior.Strict);
            ConfigurationRepositoryMock = new Mock<IConfigurationRepository>(MockBehavior.Strict);
            ConfigurationFactoryMock = new Mock<IConfigurationFactory>(MockBehavior.Strict);
            SettingsViewModelMock = new Mock<ISettingsViewModel>(MockBehavior.Strict);
        }

        protected void GivenASolutionIsOpen()
        {
            CurrentSolutionFilePath = SolutionFullName;            
        }

        protected void GivenNoSolutionIsOpen()
        {
            CurrentSolutionFilePath = string.Empty;            
        }

        protected void CreateInstanceVisualizerViewModel()
        {
            InstanceVisualizerViewModel =
                new InstanceVisualizerViewModel(
                    FileExistenceCheckerMock.Object,
                    ConfigurationRepositoryMock.Object,
                    ConfigurationFactoryMock.Object,
                    SettingsViewModelMock.Object);
        }

        protected void ActivateTheInstanceVisualizerViewModel()
        {
            InstanceVisualizerViewModel.Activate(SolutionFullName);
        }

        protected void SimulateThatTheConfigurationFilePathExists()
        {
            FileExistenceCheckerMock.Setup(x => x.Exists(It.IsAny<string>())).Returns(true);
        }

        protected void SimulateThatTheConfigurationFilePathDoesNotExists()
        {
            FileExistenceCheckerMock.Setup(x => x.Exists(It.IsAny<string>())).Returns(false);
        }

        protected void SimulateLoadingTheConfiguration()
        {
            ConfigurationRepositoryMock.Setup(x => x.Load(ConfigurationFullName)).Returns(Configuration);
        }

        protected void SimulateSavingTheConfiguration()
        {
            ConfigurationRepositoryMock.Setup(x => x.Save(ConfigurationFullName, It.IsAny<Configuration>())).Verifiable();
        }

        protected void SimulateCreatingTheDefaultConfiguration()
        {
            Configuration = new DefaultConfiguration(string.Empty);
            ConfigurationFactoryMock.Setup(x => x.CreateDefaultConfiguration(It.IsAny<string>())).Returns(Configuration);
        }

        protected void SimulateCreatingTheInformativeConfiguration()
        {
            Configuration = new InformativeConfiguration();
            ConfigurationFactoryMock.Setup(x => x.CreateInformativeConfiguration()).Returns(Configuration);
        }

        protected void GivenAConfigurationIsPreviouslySaved()
        {
            Configuration = new Configuration();
        }

        protected void GivenNoConfigurationIsPreviouslySaved()
        {
            Configuration = null;
        }

        protected void GivenASolutionFullName()
        {
            SolutionFullName = @"C:\test\x.sln";
        }

        protected void GivenAConfigurationFullName()
        {
            ConfigurationFullName = @"C:\test\x.InstanceVisualizer.xml";
        }
    }
}