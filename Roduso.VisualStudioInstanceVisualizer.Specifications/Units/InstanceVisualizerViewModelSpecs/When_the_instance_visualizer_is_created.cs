﻿using NUnit.Framework;

namespace Roduso.VisualStudioInstanceVisualizer.Specifications.Units.InstanceVisualizerViewModelSpecs
{
    [TestFixture]
    public class When_the_instance_visualizer_is_created : InstanceVisualizerViewModelContextSpecification
    {
        [Test]
        public void A_configuration_should_always_be_available()
        {
            Assert.That(InstanceVisualizerViewModel.Configuration, Is.Not.Null);
        }

        protected override void SetUpMockData()
        {
        }

        protected override void SimulateExternalActorsBehaviours()
        {
        }

        protected override void InitializeStateOnClassUnderTest()
        {
        }

        protected override void Act()
        {
            CreateInstanceVisualizerViewModel();
        }
    }
}