using Moq;

using NUnit.Framework;

using Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling;

namespace Roduso.VisualStudioInstanceVisualizer.Specifications.Units.InstanceVisualizerViewModelSpecs
{
    [TestFixture]
    public class When_the_instance_visualizer_is_initialized_without_a_solution_folder : InstanceVisualizerViewModelContextSpecification
    {
        [Test]
        public void An_informative_configuration_should_be_created()
        {
            ConfigurationFactoryMock.Verify(x => x.CreateInformativeConfiguration(), Times.Once());
        }

        [Test]
        public void The_informative_configuration_should_be_exposed()
        {
            Assert.That(InstanceVisualizerViewModel.Configuration, Is.TypeOf<InformativeConfiguration>());
        }

        protected override void SetUpMockData()
        {
            GivenNoSolutionIsOpen();
            GivenAConfigurationIsPreviouslySaved();
        }

        protected override void SimulateExternalActorsBehaviours()
        {
            SimulateThatTheConfigurationFilePathDoesNotExists();
            SimulateCreatingTheInformativeConfiguration();
        }

        protected override void InitializeStateOnClassUnderTest()
        {
            CreateInstanceVisualizerViewModel();
        }

        protected override void Act()
        {
            ActivateTheInstanceVisualizerViewModel();
        }
    }
}