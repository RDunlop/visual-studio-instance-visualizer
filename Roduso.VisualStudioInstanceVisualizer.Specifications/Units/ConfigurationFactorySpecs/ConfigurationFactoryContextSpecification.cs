using Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling;

namespace Roduso.VisualStudioInstanceVisualizer.Specifications.Units.ConfigurationFactorySpecs
{
    public abstract class ConfigurationFactoryContextSpecification : ContextSpecification
    {
        protected IConfigurationFactory ConfigurationFactory { get; set; }
        
        protected Configuration Configuration { get; set; }

        protected override void CreateMocksOfExternalActors()
        {
        }

        protected void CreateConfigurationFactory()
        {
            ConfigurationFactory = new ConfigurationFactory();
        }

        protected void CreateInformativeConfiguration()
        {
            Configuration = ConfigurationFactory.CreateInformativeConfiguration();
        }
    }
}