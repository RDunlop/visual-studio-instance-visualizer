﻿using System.Windows.Media;

using NUnit.Framework;

using Roduso.VisualStudioInstanceVisualizer.UserInterface.ColourHandling;

namespace Roduso.VisualStudioInstanceVisualizer.Specifications.Units.ConfigurationFactorySpecs
{
    [TestFixture]
    public class When_the_informative_configuration_is_created : ConfigurationFactoryContextSpecification
    {
        [Test]
        public void An_informative_text_should_be_displayed_insted_of_the_project_name()
        {
            Assert.That(Configuration.ProjectName, Is.EqualTo("No solution available"));
        }

        [Test]
        public void The_application_name_should_be_displayed_instead_of_the_instance_name()
        {
            Assert.That(Configuration.InstanceName, Is.EqualTo("Instance Visualizer"));
        }

        [Test]
        public void The_start_background_should_be_green()
        {
            ColourNameExtractor colourNameExtractor = new ColourNameExtractor();
            Assert.That(Configuration.BackgroundStartColour, Is.EqualTo(colourNameExtractor.GetKnownColorName(Colors.DarkGreen)));
        }

        [Test]
        public void The_end_background_should_be_a_paler_green()
        {
            ColourNameExtractor colourNameExtractor = new ColourNameExtractor();
            Assert.That(Configuration.BackgroundEndColour, Is.EqualTo(colourNameExtractor.GetKnownColorName(Colors.MediumSeaGreen)));
        }

        protected override void SetUpMockData()
        {
        }

        protected override void SimulateExternalActorsBehaviours()
        {
        }

        protected override void InitializeStateOnClassUnderTest()
        {
            CreateConfigurationFactory();
        }

        protected override void Act()
        {
            CreateInformativeConfiguration();
        }
    }
}