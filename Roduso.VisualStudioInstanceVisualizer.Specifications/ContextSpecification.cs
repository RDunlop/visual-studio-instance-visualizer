﻿using NUnit.Framework;

namespace Roduso.VisualStudioInstanceVisualizer.Specifications
{    
    public abstract class ContextSpecification
    {
        [SetUp]
        public void SetUp()
        {
            CreateMocksOfExternalActors();
            SetUpMockData();
            SimulateExternalActorsBehaviours();
            InitializeStateOnClassUnderTest();
            SubscribeToEvents();
            Act();
        }

        [TearDown]
        public void TearDown()
        {
            AfterEachSpecification();
        }

        protected abstract void SetUpMockData();
        protected abstract void SimulateExternalActorsBehaviours();
        protected abstract void InitializeStateOnClassUnderTest();        
        protected abstract void Act();

        protected virtual void AfterEachSpecification()
        {
        }

        protected virtual void CreateMocksOfExternalActors()
        {
        }

        protected virtual void SubscribeToEvents()
        {            
        }
    }
}