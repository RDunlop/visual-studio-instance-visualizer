﻿using System.ComponentModel;

using Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling;

namespace Roduso.VisualStudioInstanceVisualizer.UserInterface.ViewModels
{
    public class SettingsViewModel : ViewModel, ISettingsViewModel
    {
        private readonly IConfigurationRepository _configurationRepository;
        private Configuration _configuration;
        private string _configurationFilePath;

        public SettingsViewModel(IConfigurationRepository configurationRepository)
        {
            _configurationRepository = configurationRepository;
        }

        public Configuration Configuration
        {
            get { return _configuration; }
            set { AssignAndNotify(ref _configuration, value, () => Configuration); }
        }

        public void Initialize(Configuration configuration, string configurationFilePath)
        {
            _configurationFilePath = configurationFilePath;

            if (_configuration != configuration)
            {
                _configuration = configuration;
                _configuration.PropertyChanged += OnConfigurationPropertyChanged;
            }
        }

        private void OnConfigurationPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            _configurationRepository.Save(_configurationFilePath, Configuration);
        }
    }
}