using System;
using System.ComponentModel;
using System.Linq.Expressions;

namespace Roduso.VisualStudioInstanceVisualizer.UserInterface.ViewModels
{
    public interface INotifyPropertyChangedSupportingBase : INotifyPropertyChanged
    {
        void SendAllPropertiesChanged();
        string GetPropertyName<T>(Expression<Func<T>> exp);
    }
}