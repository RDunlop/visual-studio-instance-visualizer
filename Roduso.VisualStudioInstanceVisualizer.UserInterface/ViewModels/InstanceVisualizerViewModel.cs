﻿using Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling;
using Roduso.VisualStudioInstanceVisualizer.UserInterface.FileHandling;

using Configuration = Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling.Configuration;

namespace Roduso.VisualStudioInstanceVisualizer.UserInterface.ViewModels
{
    public class InstanceVisualizerViewModel : ViewModel, IInstanceVisualizerViewModel
    {
        private readonly IFileExistenceChecker _fileExistenceChecker;
        private readonly IConfigurationRepository _configurationRepository;
        private readonly IConfigurationFactory _configurationFactory;
        private readonly ISettingsViewModel _settingsViewModel;

        private Configuration _configuration;
        private string _currentSolutionFullName;

        public InstanceVisualizerViewModel(
            IFileExistenceChecker fileExistenceChecker,
            IConfigurationRepository configurationRepository,
            IConfigurationFactory configurationFactory,
            ISettingsViewModel settingsViewModel)
        {
            _fileExistenceChecker = fileExistenceChecker;
            _configurationRepository = configurationRepository;
            _configurationFactory = configurationFactory;

            _settingsViewModel = settingsViewModel;

            Configuration = new InformativeConfiguration();
        }

        public Configuration Configuration
        {
            get { return _configuration; }
            set { AssignAndNotify(ref _configuration, value, () => Configuration, () => Configuration.ProjectName, () => CanEdit); }
        }

        public bool CanEdit => string.IsNullOrEmpty(CurrentSolutionFullName) == false;
        private bool IsOpening => string.IsNullOrEmpty(CurrentSolutionFullName) == false;

        private string ConfigurationFullName => _currentSolutionFullName.Replace(".sln", string.Empty) + ".InstanceVisualizer.xml";

        private string CurrentSolutionFullName
        {
            get { return _currentSolutionFullName; }
            set { AssignAndNotify(ref _currentSolutionFullName, value, () => CurrentSolutionFullName, () => CanEdit); }
        }

        public void Activate(string solutionFullName)
        {
            if (solutionFullName == null)
            {
                ShowInformativeConfiguration();
            }
            else
            {
                SolutionChanged(solutionFullName);
            }
        }

        public void SolutionChanged(string solutionFullName)
        {
            if (CurrentSolutionFullName == solutionFullName)
            {
                return;
            }

            CurrentSolutionFullName = solutionFullName;            

            if (IsOpening)
            {
                ReadConfiguration(CurrentSolutionFullName);
            }
            else
            {
                ShowInformativeConfiguration();
            }
        }

        public void SolutionClosed()
        {
            ShowInformativeConfiguration();
        }

        public ISettingsViewModel GetSettingsViewModel()
        {
            _settingsViewModel.Initialize(Configuration, ConfigurationFullName);
            return _settingsViewModel;
        }

        private void ShowInformativeConfiguration()
        {
            CurrentSolutionFullName = string.Empty;
            Configuration = _configurationFactory.CreateInformativeConfiguration();
        }

        private void ReadConfiguration(string solutionFullName)
        {
            if (string.IsNullOrEmpty(CurrentSolutionFullName))
            {
                return;
            }

            if (_fileExistenceChecker.Exists(ConfigurationFullName) == false)
            {
                Configuration = _configurationFactory.CreateDefaultConfiguration(solutionFullName);
                _configurationRepository.Save(ConfigurationFullName, Configuration);                
            }
            else
            {
                Configuration = _configurationRepository.Load(ConfigurationFullName);
            }
        }
    }
}