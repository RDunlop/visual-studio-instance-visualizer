﻿using Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling;
using Roduso.VisualStudioInstanceVisualizer.UserInterface.FileHandling;

namespace Roduso.VisualStudioInstanceVisualizer.UserInterface.ViewModels
{
    public class InstanceVisualizerViewModelFactory
    {
        public static InstanceVisualizerViewModel CreateGraph()
        {
            return new InstanceVisualizerViewModel(
                new FileExistenceChecker(),
                new ConfigurationRepository(),
                new ConfigurationFactory(),
                new SettingsViewModel(new ConfigurationRepository()));
        }
    }
}