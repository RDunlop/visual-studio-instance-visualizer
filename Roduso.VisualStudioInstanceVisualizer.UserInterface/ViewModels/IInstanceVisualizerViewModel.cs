using Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling;

namespace Roduso.VisualStudioInstanceVisualizer.UserInterface.ViewModels
{
    public interface IInstanceVisualizerViewModel
    {
        Configuration Configuration { get; set; }

        void Activate(string solutionFullName);
        void SolutionChanged(string solutionFullName);
        void SolutionClosed();
        ISettingsViewModel GetSettingsViewModel();
    }
}