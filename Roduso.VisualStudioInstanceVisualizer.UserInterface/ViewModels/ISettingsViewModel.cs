using System.ComponentModel;

using Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling;

namespace Roduso.VisualStudioInstanceVisualizer.UserInterface.ViewModels
{
    public interface ISettingsViewModel
    {
        event PropertyChangedEventHandler PropertyChanged;
        Configuration Configuration { get; }
        void Initialize(Configuration configuration, string configurationFilePath);
    }
}