﻿using System.Windows.Media;

using Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling;

namespace Roduso.VisualStudioInstanceVisualizer.UserInterface.ViewModels
{
    public class VisualizerViewModel : ViewModel
    {
        private Configuration _configuration;
        
        public Configuration Configuration
        {
            get { return _configuration; }
            set { AssignAndNotify(ref _configuration, value, () => Configuration); }
        }

        public string ProjectName
        {
            get
            {
                return Configuration.ProjectName;
            }
            set
            {
                string projectName = Configuration.ProjectName;
                AssignAndNotify(ref projectName, value, () => ProjectName);
            }
        }

        public Brush ProjectForeground => Configuration.ProjectForeground;
    }
}