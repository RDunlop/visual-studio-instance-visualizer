using System;
using System.Runtime.Serialization;
using System.Xml;

namespace Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling
{
    public class ConfigurationRepository : IConfigurationRepository
    {
        public Configuration Load(string configurationFilePath)
        {
            using (XmlReader xmlReader = XmlReader.Create(configurationFilePath))
            {
                try
                {
                    DataContractSerializer serializer = new DataContractSerializer(typeof(Configuration));
                    return serializer.ReadObject(xmlReader) as Configuration;
                }
                catch (Exception)
                {
                    return new DefaultConfiguration(string.Empty);
                }
            }
        }

        public void Save(string configurationFilePath, Configuration configuration)
        {
            CustomConfiguration customConfiguration = new CustomConfiguration(configuration);
            using (XmlWriter writer = XmlWriter.Create(configurationFilePath))
            {
                try
                {
                    DataContractSerializer serializer = new DataContractSerializer(typeof(Configuration));
                    serializer.WriteObject(writer, customConfiguration);
                }
                catch (Exception)
                {
                    return;
                }
            }
        }
    }
}