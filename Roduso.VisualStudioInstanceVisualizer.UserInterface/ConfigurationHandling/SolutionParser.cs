using System;
using System.Linq;

namespace Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling
{
    public class SolutionParser
    {
        public static string GetSolutionFolder(string solutionFullName)
        {
            try
            {
                string solutionDir = System.IO.Path.GetDirectoryName(solutionFullName);
                return solutionDir;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        public static string GetSolutionFileName(string solutionFullName)
        {
            var splitResult = solutionFullName.Split(new[] { '\\' });
            return splitResult.Last();
        }

        public static string GetSolutionName(string solutionFullName)
        {            
            return GetSolutionFileName(solutionFullName).Replace(".sln", string.Empty);
        }
    }
}