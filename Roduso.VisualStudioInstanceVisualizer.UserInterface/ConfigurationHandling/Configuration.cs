using System;
using System.Runtime.Serialization;
using System.Windows.Media;

using Roduso.VisualStudioInstanceVisualizer.UserInterface.ColourHandling;

namespace Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling
{
    [DataContract]
    [KnownType(typeof(LinearGradientBrush))]
    [KnownType(typeof(DefaultConfiguration))]
    [KnownType(typeof(InformativeConfiguration))]
    [KnownType(typeof(CustomConfiguration))]
    public class Configuration : NotifyPropertyChangedSupportingBase
    {
        private IColourNameExtractor _colourNameExtractor = new ColourNameExtractor();
        private string _projectName;
        private string _instanceName;
        private string _projectForegroundColour;
        private string _instanceForegroundColour;
        private string _fontFamily;
        private int _fontSize;
        private string _backgroundStartColour;
        private string _backgroundEndColour;

        [DataMember]
        public string ProjectName
        {
            get { return _projectName; }
            set { AssignAndNotify(ref _projectName, value, () => ProjectName); }
        }

        [DataMember]
        public string InstanceName
        {
            get { return _instanceName; }
            set { AssignAndNotify(ref _instanceName, value, () => InstanceName); }
        }

        [DataMember]
        public string ProjectForegroundColour
        {
            get { return _projectForegroundColour; }
            set { AssignAndNotify(ref _projectForegroundColour, value, () => ProjectForegroundColour, () => ProjectForeground); }
        }

        [DataMember]
        public string InstanceForegroundColour
        {
            get { return _instanceForegroundColour; }
            set { AssignAndNotify(ref _instanceForegroundColour, value, () => InstanceForegroundColour, () => InstanceForeground); }
        }

        [DataMember]
        public string FontFamily
        {
            get { return _fontFamily; }
            set { AssignAndNotify(ref _fontFamily, value, () => FontFamily); }
        }

        [DataMember]
        public int FontSize
        {
            get { return _fontSize; }
            set { AssignAndNotify(ref _fontSize, value, () => FontSize); }
        }

        [DataMember]
        public string BackgroundStartColour
        {
            get { return _backgroundStartColour; }
            set { AssignAndNotify(ref _backgroundStartColour, value, () => BackgroundStartColour, () => Background); }
        }

        [DataMember]
        public string BackgroundEndColour
        {
            get { return _backgroundEndColour; }
            set { AssignAndNotify(ref _backgroundEndColour, value, () => BackgroundEndColour, () => Background); }
        }

        [IgnoreDataMember]
        public Brush Background => FromColourName(BackgroundStartColour, BackgroundEndColour);

        [IgnoreDataMember]
        public Brush ProjectForeground => FromColourName(ProjectForegroundColour);

        [IgnoreDataMember]
        public Brush InstanceForeground => FromColourName(InstanceForegroundColour);

        public IColourNameExtractor ColourNameExtractor
        {
            get { return _colourNameExtractor; }
            set { AssignAndNotify(ref _colourNameExtractor, value, () => ColourNameExtractor); }
        }

        // ToDo:  Move to colour handling 2012-12-16
        private static Color SafeConvertFromString(string colourName, Color defaultColour)
        {
            try
            {
                var convertFromString = ColorConverter.ConvertFromString(colourName);

                if (convertFromString != null)
                {
                    return (Color)convertFromString;
                }

                return defaultColour;
            }
            catch (Exception)
            {
                return defaultColour;
            }
        }

        private SolidColorBrush FromColourName(string colourName)
        {
            Color colour = SafeConvertFromString(colourName, Colors.WhiteSmoke);
            return new SolidColorBrush(colour);
        }        

        // ToDo:  Move to colour handling 2012-12-16
        private LinearGradientBrush FromColourName(string startColourName, string endColourName)
        {
            Color startColour = SafeConvertFromString(startColourName, Colors.DarkRed);
            Color endColour = SafeConvertFromString(endColourName, Colors.Red);

            return new LinearGradientBrush(startColour, endColour, 90.0);
        }
    }
}