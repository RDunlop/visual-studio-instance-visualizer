namespace Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling
{
    public class ConfigurationFactory : IConfigurationFactory
    {
        public Configuration CreateDefaultConfiguration(string solutionFolder)
        {
            return new DefaultConfiguration(solutionFolder);
        }

        public Configuration CreateInformativeConfiguration()
        {
            return new InformativeConfiguration();
        }
    }
}