namespace Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling
{
    public interface IConfigurationRepository
    {
        Configuration Load(string configurationFilePath);
        void Save(string configurationFilePath, Configuration configuration);
    }
}