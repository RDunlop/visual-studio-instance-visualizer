using System.Runtime.Serialization;
using System.Windows.Media;

namespace Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling
{
    [DataContract]
    public class InformativeConfiguration : Configuration
    {        
        public InformativeConfiguration()
        {
            ProjectName = "No solution available";
            InstanceName = "Instance Visualizer";
            BackgroundStartColour = ColourNameExtractor.GetKnownColorName(Colors.DarkGreen);
            BackgroundEndColour = ColourNameExtractor.GetKnownColorName(Colors.MediumSeaGreen);
            ProjectForegroundColour = ColourNameExtractor.GetKnownColorName(Colors.Gold);
            InstanceForegroundColour = ColourNameExtractor.GetKnownColorName(Colors.WhiteSmoke);
            FontFamily = "Verdana";
            FontSize = 15;
        }
    }
}