using System.Runtime.Serialization;
using System.Windows.Media;

namespace Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling
{
    [DataContract]
    public class DefaultConfiguration : Configuration
    {
        public DefaultConfiguration(string solutionFullName)
        {
            ProjectName = string.IsNullOrEmpty(solutionFullName) ? "Solution" : SolutionParser.GetSolutionName(solutionFullName);
            InstanceName = "Yet to be named";
            BackgroundStartColour = ColourNameExtractor.GetKnownColorName(Colors.DimGray);
            BackgroundEndColour = ColourNameExtractor.GetKnownColorName(Colors.Black);
            ProjectForegroundColour = ColourNameExtractor.GetKnownColorName(Colors.PapayaWhip);
            InstanceForegroundColour = ColourNameExtractor.GetKnownColorName(Colors.WhiteSmoke);
            FontFamily = "Verdana";
            FontSize = 15;
        }
    }
}