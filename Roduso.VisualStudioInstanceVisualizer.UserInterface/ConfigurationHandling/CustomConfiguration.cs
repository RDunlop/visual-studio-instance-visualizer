using System.Runtime.Serialization;

namespace Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling
{
    [DataContract]
    public class CustomConfiguration : Configuration
    {
        public CustomConfiguration(Configuration configuration)
        {
            BackgroundStartColour = configuration.BackgroundStartColour;
            BackgroundEndColour = configuration.BackgroundEndColour;
            InstanceForegroundColour = configuration.InstanceForegroundColour;
            InstanceName = configuration.InstanceName;
            ProjectForegroundColour = configuration.ProjectForegroundColour;
            ProjectName = configuration.ProjectName;
            FontFamily = configuration.FontFamily;
            FontSize = configuration.FontSize;
        }
    }
}