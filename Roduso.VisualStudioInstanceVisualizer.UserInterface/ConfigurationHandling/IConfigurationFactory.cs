namespace Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling
{
    public interface IConfigurationFactory
    {
        Configuration CreateDefaultConfiguration(string solutionFolder);
        Configuration CreateInformativeConfiguration();
    }
}