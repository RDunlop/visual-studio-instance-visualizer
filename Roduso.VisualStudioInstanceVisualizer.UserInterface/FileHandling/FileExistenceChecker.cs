using System.IO;

namespace Roduso.VisualStudioInstanceVisualizer.UserInterface.FileHandling
{
    public class FileExistenceChecker : IFileExistenceChecker
    {
        public bool Exists(string filePath)
        {
            return File.Exists(filePath);
        }
    }
}