namespace Roduso.VisualStudioInstanceVisualizer.UserInterface.FileHandling
{
    public interface IFileExistenceChecker
    {
        bool Exists(string filePath);
    }
}