﻿using System.Windows;

using Roduso.VisualStudioInstanceVisualizer.UserInterface.ViewModels;

namespace Roduso.VisualStudioInstanceVisualizer.UserInterface.Views
{
    public partial class InstanceVisualizer
    {
        public InstanceVisualizer()
        {
            InitializeComponent();
            InstanceVisualizerViewModel = InstanceVisualizerViewModelFactory.CreateGraph();
        }

        public InstanceVisualizerViewModel InstanceVisualizerViewModel
        {
            get { return (InstanceVisualizerViewModel)DataContext; }
            set { DataContext = value; }
        }

        private void OnSettingsClicked(object sender, RoutedEventArgs e)
        {
            SettingsWindow settingsWindow = new SettingsWindow
            {
                DataContext = InstanceVisualizerViewModel.GetSettingsViewModel()
            };

            settingsWindow.ShowDialog();
        }
    }
}
