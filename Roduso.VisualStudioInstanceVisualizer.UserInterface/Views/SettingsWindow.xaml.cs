﻿using Roduso.VisualStudioInstanceVisualizer.UserInterface.ViewModels;

namespace Roduso.VisualStudioInstanceVisualizer.UserInterface.Views
{
    public partial class SettingsWindow
    {
        public SettingsWindow()
        {
            InitializeComponent();
        }

        public SettingsViewModel SettingsViewModel
        {
            get { return (SettingsViewModel)DataContext; }
            set { DataContext = value; }
        }
    }
}
