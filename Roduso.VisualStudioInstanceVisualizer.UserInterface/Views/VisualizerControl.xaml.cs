﻿using Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling;

namespace Roduso.VisualStudioInstanceVisualizer.UserInterface.Views
{
    public partial class VisualizerControl
    {
        public VisualizerControl()
        {
            InitializeComponent();
            DataContext = Configuration;
        }

        public Configuration Configuration
        {
            get { return (Configuration)DataContext; }
            set { DataContext = value; }
        }
    }
}
