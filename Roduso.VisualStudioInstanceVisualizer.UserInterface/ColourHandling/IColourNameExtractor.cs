using System.Windows.Media;

namespace Roduso.VisualStudioInstanceVisualizer.UserInterface.ColourHandling
{
    public interface IColourNameExtractor
    {
        string GetKnownColorName(Color colour);
    }
}