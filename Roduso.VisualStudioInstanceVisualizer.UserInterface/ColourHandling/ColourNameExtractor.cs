﻿using System;
using System.Reflection;
using System.Windows.Media;

namespace Roduso.VisualStudioInstanceVisualizer.UserInterface.ColourHandling
{
    public class ColourNameExtractor : IColourNameExtractor
    {
        public string GetKnownColorName(Color colour)
        {
            Type colorType = typeof(Colors);
            PropertyInfo[] colorProperties = colorType.GetProperties(BindingFlags.Public | BindingFlags.Static);

            foreach (PropertyInfo colourProperty in colorProperties)
            {
                Color knownColor = (Color)colourProperty.GetValue(null, null);
                if (knownColor == colour)
                {
                    return colourProperty.Name;
                }
            }

            return string.Empty;
        } 
    }
}