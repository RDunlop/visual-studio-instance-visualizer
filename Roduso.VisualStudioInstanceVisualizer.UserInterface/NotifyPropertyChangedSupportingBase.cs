using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Runtime.Serialization;

using Roduso.VisualStudioInstanceVisualizer.UserInterface.ViewModels;

namespace Roduso.VisualStudioInstanceVisualizer.UserInterface
{
    [Serializable]
    [DataContract]
    public abstract class NotifyPropertyChangedSupportingBase : INotifyPropertyChangedSupportingBase
    {
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        public void SendAllPropertiesChanged()
        {
            SendPropertyChanged(null);
        }

        public string GetPropertyName<T>(Expression<Func<T>> exp)
        {
            MemberExpression body = exp.Body as MemberExpression;

            if (body == null)
            {
                UnaryExpression ubody = (UnaryExpression)exp.Body;
                body = ubody.Operand as MemberExpression;
            }

            if (body != null)
            {
                return body.Member.Name;
            }

            return string.Empty;
        }

        protected void SendPropertyChanged<T>(Expression<Func<T>> property)
        {
            string propertyName = GetPropertyName(property);
            SendPropertyChanged(propertyName);
        }

        protected virtual void OnPropertyChanged()
        {
        }

        protected void AssignAndNotifyAll<T>(ref T field, T value)
        {
            if (!EqualityComparer<T>.Default.Equals(field, value))
            {
                field = value;
                SendAllPropertiesChanged();
            }
        }

        protected void AssignAndNotify<T>(ref T field, T value, Expression<Func<T>> property)
        {
            if (!EqualityComparer<T>.Default.Equals(field, value))
            {
                field = value;
                SendPropertyChanged(property);
            }
        }

        protected void AssignAndNotify<T>(ref T field, T value, params Expression<Func<object>>[] properties)
        {
            if (!EqualityComparer<T>.Default.Equals(field, value))
            {
                field = value;
                foreach (Expression<Func<object>> property in properties)
                {
                    SendPropertyChanged(property);
                }
            }
        }

        private void SendPropertyChanged(string propertyName)
        {
            OnPropertyChanged();

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}