﻿using EnvDTE;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;

namespace Roduso.VisualStudioInstanceVisualizer
{
    public class DefaultToolWindowPane : ToolWindowPane, IVsSolutionEvents, IVsWindowFrameNotify3
    {
        private uint _cookie;
        private IVsSolution _solutionService;

        public DefaultToolWindowPane() : base(null)
        {
        }

        protected string SolutionFileName => ServiceAsDte?.Solution?.FileName;
        private DTE ServiceAsDte => (DTE)GetService(typeof(DTE));

        public virtual int OnAfterOpenProject(IVsHierarchy pHierarchy, int fAdded)
        {
            return VSConstants.S_OK;
        }

        public virtual int OnQueryCloseProject(IVsHierarchy pHierarchy, int fRemoving, ref int pfCancel)
        {
            return VSConstants.S_OK;
        }

        public virtual int OnBeforeCloseProject(IVsHierarchy pHierarchy, int fRemoved)
        {
            return VSConstants.S_OK;
        }

        public virtual int OnAfterLoadProject(IVsHierarchy pStubHierarchy, IVsHierarchy pRealHierarchy)
        {
            return VSConstants.S_OK;
        }

        public virtual int OnQueryUnloadProject(IVsHierarchy pRealHierarchy, ref int pfCancel)
        {
            return VSConstants.S_OK;
        }

        public virtual int OnBeforeUnloadProject(IVsHierarchy pRealHierarchy, IVsHierarchy pStubHierarchy)
        {
            return VSConstants.S_OK;
        }

        public virtual int OnAfterOpenSolution(object pUnkReserved, int fNewSolution)
        {
            return VSConstants.S_OK;
        }

        public virtual int OnQueryCloseSolution(object pUnkReserved, ref int pfCancel)
        {
            return VSConstants.S_OK;
        }

        public virtual int OnBeforeCloseSolution(object pUnkReserved)
        {
            return VSConstants.S_OK;
        }

        public virtual int OnAfterCloseSolution(object pUnkReserved)
        {
            return VSConstants.S_OK;
        }

        public int OnShow(int fShow)
        {
            __FRAMESHOW show = (__FRAMESHOW)fShow;

            switch (show)
            {
                case __FRAMESHOW.FRAMESHOW_Hidden:
                    break;
                case __FRAMESHOW.FRAMESHOW_WinShown:
                    OnToolWindowShown();
                    break;
                case __FRAMESHOW.FRAMESHOW_TabActivated:
                    break;
                case __FRAMESHOW.FRAMESHOW_TabDeactivated:
                    break;
                case __FRAMESHOW.FRAMESHOW_WinRestored:
                    break;
                case __FRAMESHOW.FRAMESHOW_WinMinimized:
                    break;
                case __FRAMESHOW.FRAMESHOW_WinMaximized:
                    break;
                case __FRAMESHOW.FRAMESHOW_WinClosed:
                    break;
                case __FRAMESHOW.FRAMESHOW_DestroyMultInst:
                    break;
                case __FRAMESHOW.FRAMESHOW_AutoHideSlideBegin:
                    break;
                default:
                    break;
            }

            return VSConstants.S_OK;
        }

        protected virtual void OnToolWindowShown()
        {
        }

        public int OnMove(int x, int y, int w, int h)
        {
            return VSConstants.S_OK;
        }

        public int OnSize(int x, int y, int w, int h)
        {
            return VSConstants.S_OK;
        }

        public int OnDockableChange(int fDockable, int x, int y, int w, int h)
        {
            return VSConstants.S_OK;
        }

        public int OnClose(ref uint pgrfSaveOptions)
        {
            return VSConstants.S_OK;
        }

        public override void OnToolWindowCreated()
        {
            base.OnToolWindowCreated();
        }

        protected override void OnClose()
        {
            base.OnClose();
        }

        protected override void OnCreate()
        {
            base.OnCreate();
        }

        protected override void Initialize()
        {
            _solutionService = GetService(typeof(SVsSolution)) as IVsSolution;
            _solutionService?.AdviseSolutionEvents(this, out _cookie);
        }

        protected override void Dispose(bool disposing)
        {
            _solutionService?.UnadviseSolutionEvents(_cookie);
            base.Dispose(disposing);
        }
    }
}