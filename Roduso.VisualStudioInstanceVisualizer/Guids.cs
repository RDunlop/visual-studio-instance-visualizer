﻿using System;

namespace Roduso.VisualStudioInstanceVisualizer.Toolbox
{
    public static class GuidList
    {
        public const string GuidRodusoVisualStudioInstanceVisualizerPkgString = "3e93da10-1c42-4fd2-8ea5-1b54a50ded64";
        public const string GuidRodusoVisualStudioInstanceVisualizerCmdSetString = "fad76d6d-a57d-4a4e-9796-0010cef56411";
        public const string GuidToolWindowPersistanceString = "b69c0e74-851d-4c69-ab75-a565c12d7f98";

        public static readonly Guid GuidRodusoVisualStudioInstanceVisualizerCmdSet = new Guid(GuidRodusoVisualStudioInstanceVisualizerCmdSetString);
    }
}