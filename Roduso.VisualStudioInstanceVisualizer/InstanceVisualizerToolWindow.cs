﻿using System.Runtime.InteropServices;
using Roduso.VisualStudioInstanceVisualizer.UserInterface.Views;

namespace Roduso.VisualStudioInstanceVisualizer
{
    [Guid("b69c0e74-851d-4c69-ab75-a565c12d7f98")]
    public sealed class InstanceVisualizerToolWindow : DefaultToolWindowPane
    {
        private InstanceVisualizer _instanceVisualizer;

        private InstanceVisualizerToolViewModel _instanceVisualizerToolViewModel;

        public InstanceVisualizerToolWindow()
        {           
            Caption = "Instance Visualizer";
            BitmapResourceID = 301;
            BitmapIndex = 1;
        }

        protected override void Initialize()
        {
            base.Initialize();

            SetInstanceVisualizerAsContent();
            InitializeViewModel();
        }

        public override void OnToolWindowCreated()
        {
            _instanceVisualizerToolViewModel?.Activated(SolutionFileName);
            base.OnToolWindowCreated();
        }

        public override int OnAfterOpenSolution(object pUnkReserved, int fNewSolution)
        {
            _instanceVisualizerToolViewModel?.SolutionChanged(SolutionFileName);
            return base.OnAfterOpenSolution(pUnkReserved, fNewSolution);
        }

        public override int OnBeforeCloseSolution(object pUnkReserved)
        {
            _instanceVisualizerToolViewModel?.SolutionClosed();
            return base.OnBeforeCloseSolution(pUnkReserved);
        }

        private void InitializeViewModel()
        {
            _instanceVisualizerToolViewModel = 
                new InstanceVisualizerToolViewModel(_instanceVisualizer.InstanceVisualizerViewModel);
        }

        private void SetInstanceVisualizerAsContent()
        {
            _instanceVisualizer = new InstanceVisualizer();
            Content = _instanceVisualizer;
        }
    }
}
