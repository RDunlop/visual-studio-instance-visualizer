﻿using Roduso.VisualStudioInstanceVisualizer.UserInterface.ViewModels;

namespace Roduso.VisualStudioInstanceVisualizer
{
    public class InstanceVisualizerToolViewModel : ViewModel
    {
        public InstanceVisualizerToolViewModel(IInstanceVisualizerViewModel instanceVisualizerViewModel)
        {
            InstanceVisualizerViewModel = instanceVisualizerViewModel;
        }

        public IInstanceVisualizerViewModel InstanceVisualizerViewModel { get; private set; }       

        public void Activated(string solutionFullName)
        {
            InstanceVisualizerViewModel.Activate(solutionFullName);
        }

        public void SolutionChanged(string solutionFullName)
        {
            InstanceVisualizerViewModel.SolutionChanged(solutionFullName);
        }

        public void SolutionClosed()
        {
            InstanceVisualizerViewModel.SolutionClosed();
        }
    }
}