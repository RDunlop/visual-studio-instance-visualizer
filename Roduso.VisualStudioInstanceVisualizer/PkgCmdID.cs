﻿// PkgCmdID.cs
// MUST match PkgCmdID.h

namespace Roduso.VisualStudioInstanceVisualizer.Toolbox
{
    public static class PkgCmdIDList
    {
        public const uint CmdidVsInstanceVisualizer = 0x101;
    }
}