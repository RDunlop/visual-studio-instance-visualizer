﻿using Roduso.VisualStudioInstanceVisualizer.UserInterface.ConfigurationHandling;
using Roduso.VisualStudioInstanceVisualizer.UserInterface.FileHandling;
using Roduso.VisualStudioInstanceVisualizer.UserInterface.ViewModels;

namespace Roduso.VisualStudioInstanceVisualizer.TestApp
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            InstanceVisualizerViewModel = 
                new InstanceVisualizerViewModel(
                    new FileExistenceChecker(),
                    new ConfigurationRepository(),
                    new ConfigurationFactory(),
                    new SettingsViewModel(new ConfigurationRepository()));
            InstanceVisualizerViewModel.SolutionChanged(string.Empty);
        }

        public InstanceVisualizerViewModel InstanceVisualizerViewModel
        {
            get { return (InstanceVisualizerViewModel)DataContext; }
            set { DataContext = value; }
        }

        private void OnFirstClicked(object sender, System.Windows.RoutedEventArgs e)
        {
            InstanceVisualizerViewModel.SolutionChanged(@"c:\develop\ConsoleApplication1");
        }
        
        private void OnSecondClicked(object sender, System.Windows.RoutedEventArgs e)
        {
            InstanceVisualizerViewModel.SolutionChanged(@"c:\develop\ConsoleApplication2");
        }
    }
}
